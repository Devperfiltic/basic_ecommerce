package com.basicecommerce.service.api;

import com.basicecommerce.commons.GenericServiceAPI;
import com.basicecommerce.model.Subcategoria;

public interface SubcategoriaServiceAPI extends GenericServiceAPI<Subcategoria, Integer>  {
	
}
