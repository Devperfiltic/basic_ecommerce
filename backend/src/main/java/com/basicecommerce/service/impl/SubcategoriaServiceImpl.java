package com.basicecommerce.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.basicecommerce.commons.GenericServiceImpl;
import com.basicecommerce.model.Subcategoria;
import com.basicecommerce.service.api.SubcategoriaServiceAPI;
import com.basicecommerce.dao.api.SubcategoriaDaoAPI;

@Service
public class SubcategoriaServiceImpl extends GenericServiceImpl<Subcategoria, Integer> implements SubcategoriaServiceAPI {

	@Autowired
	private SubcategoriaDaoAPI subcategoriaDaoAPI;
	
	@Override
	public JpaRepository<Subcategoria, Integer> getDao() {
		return subcategoriaDaoAPI;
	}

}
