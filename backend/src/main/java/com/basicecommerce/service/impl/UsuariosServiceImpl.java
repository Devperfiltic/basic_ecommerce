package com.basicecommerce.service.impl;

import com.basicecommerce.commons.GenericServiceImpl;
import com.basicecommerce.dao.api.UsuariosDaoAPI;
import com.basicecommerce.model.Usuarios;
import com.basicecommerce.service.api.UsuariosServiceAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

/**
 * @author Sócrates Muñoz
 */
@Service
public class UsuariosServiceImpl extends GenericServiceImpl<Usuarios, Integer> implements UsuariosServiceAPI {

	@Autowired
	private UsuariosDaoAPI usuariosDaoAPI;
	
	@Override
	public JpaRepository<Usuarios, Integer> getDao() {
		return usuariosDaoAPI;
	}

}