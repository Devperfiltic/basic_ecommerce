package com.basicecommerce.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.basicecommerce.commons.GenericServiceImpl;
import com.basicecommerce.model.Categoria;
import com.basicecommerce.service.api.CategoriaServiceAPI;
import com.basicecommerce.dao.api.CategoriaDaoAPI;

@Service
public class CategoriaServiceImpl extends GenericServiceImpl<Categoria, Integer> implements CategoriaServiceAPI {

	@Autowired
	private CategoriaDaoAPI categoriaDaoAPI;
	
	@Override
	public JpaRepository<Categoria, Integer> getDao() {
		return categoriaDaoAPI;
	}

}
