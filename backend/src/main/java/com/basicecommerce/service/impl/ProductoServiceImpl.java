package com.basicecommerce.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.basicecommerce.commons.GenericServiceImpl;
import com.basicecommerce.model.Producto;
import com.basicecommerce.service.api.ProductoServiceAPI;
import com.basicecommerce.dao.api.ProductoDaoAPI;

@Service
public class ProductoServiceImpl extends GenericServiceImpl<Producto, Integer> implements ProductoServiceAPI {

	@Autowired
	private ProductoDaoAPI productoDaoAPI;
	
	@Override
	public JpaRepository<Producto, Integer> getDao() {
		return productoDaoAPI;
	}

}
