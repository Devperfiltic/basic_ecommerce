package com.basicecommerce.dao.api;

//import org.springframework.data.repository.CrudRepository;

import com.basicecommerce.model.Subcategoria;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubcategoriaDaoAPI extends JpaRepository<Subcategoria, Integer> {

}
