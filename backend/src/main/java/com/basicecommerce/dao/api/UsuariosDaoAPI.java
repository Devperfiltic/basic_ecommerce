package com.basicecommerce.dao.api;

import com.basicecommerce.model.Usuarios;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Sócrates Muñoz
 */

public interface UsuariosDaoAPI extends JpaRepository<Usuarios, Integer> {

    public Usuarios findBynombre(String username);

}