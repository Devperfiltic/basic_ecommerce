package com.basicecommerce.dao.api;

import com.basicecommerce.model.Producto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductoDaoAPI extends JpaRepository<Producto, Integer> {

}
