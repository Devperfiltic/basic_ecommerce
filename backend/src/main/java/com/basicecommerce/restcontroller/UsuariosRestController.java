package com.basicecommerce.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.basicecommerce.model.Usuarios;
import com.basicecommerce.service.api.UsuariosServiceAPI;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@RequestMapping(value = "/apibasicecommerce/usuarios")
@CrossOrigin({"*"})
public class UsuariosRestController {

	@Autowired
	private UsuariosServiceAPI usuariosServiceAPI;

	@GetMapping(value = "/all")
	public List<Usuarios> getAll() {
		return usuariosServiceAPI.getAll();
	}
	
	@GetMapping(value = "/find/{id}")
	public Usuarios find(@PathVariable int id) {
		return usuariosServiceAPI.get(id);
	}

	@PostMapping(value = "/save")
	public ResponseEntity<Usuarios> save(@RequestBody Usuarios usuarios) {
		Usuarios obj = usuariosServiceAPI.save(usuarios);
		return new ResponseEntity<Usuarios>(obj, HttpStatus.OK);
	}

	@GetMapping(value = "/delete/{id}")
	public ResponseEntity<Usuarios> delete(@PathVariable int id) {
		Usuarios usuarios = usuariosServiceAPI.get(id);
		if (usuarios != null) {
			usuariosServiceAPI.delete(id);
		}else {
			return new ResponseEntity<Usuarios>(usuarios, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<Usuarios>(usuarios, HttpStatus.OK);
	}

}
