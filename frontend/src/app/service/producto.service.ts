import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Producto } from 'src/model/producto';

@Injectable({
  providedIn: 'root'
})

export class ProductoService {

baseUrl:string="http://localhost:8080/apibasicecommerce/productos";

  constructor(private http:HttpClient ) { }

  getAll(): Observable<any>{
  return this.http.get(this.baseUrl + "/all");
}

guardarproducto(producto: Producto): Observable<any>{
  let headers = new HttpHeaders();
  headers = headers.set('Content-Type', 'application/json');
  return this.http.post(this.baseUrl +"/save", JSON.stringify(producto), {headers: headers});
}

}


