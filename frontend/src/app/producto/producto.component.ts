import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/model/producto';
import { ProductoService } from '../service/producto.service';
import { SelectItem } from 'primeng/api/selectitem';
import { MenuItem } from 'primeng/api';
import { Command } from 'protractor';
import { Menubar } from 'primeng/menubar/menubar';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

  productos: Producto[];
  displayDialog: boolean;
  sortOptions: SelectItem[];
  sortField: string;
  sortOrder: number;

  items: MenuItem[];

  displayadmin: boolean=false;
  displayadminproductos: boolean=false;

  producto: Producto={
        idproducto: null, 
        fk_to_subcategoria: null,
        nombreproducto: null, 
        descripcion: null, 
        peso: null,
        preciousd: null,
        foto1producto: null,
        foto2producto: null,
        foto3producto: null
  }

  sortKey: string;
  
  constructor(private productoService: ProductoService) { }

  showSaveDialog(){
    this.displayadmin=true;
  }

  adiminstrarproductos(){
    this.showadiminstrarproductos()
   
  }

  showadiminstrarproductos(){
    this.displayadminproductos=true;
  }
  save(){
    console.log("mira")
  }

  guardarproducto() {
    this.productoService.guardarproducto(this.producto).subscribe(
      (result: any) => {
        console.log(result);

      },
      error => {
        console.log(error);
      }
    );
  }

  ngOnInit() {
    this.productoService.getAll().subscribe(productos => this.productos = productos);

    this.sortOptions = [
        {label: 'Nombre', value: 'nombreproducto'},
        {label: 'Precio USD', value: 'preciousd'},
        {label: 'Peso', value: 'peso'}
    ];

    this.items =[
      {
      label: "Administrar",
      icon: "pi pi-user-plus",
      command: ()=> this.showSaveDialog()
 
    }
    ]

  }

onSortChange(event) {
    let value = event.value;

    if (value.indexOf('!') === 0) {
        this.sortOrder = -1;
        this.sortField = value.substring(1, value.length);
    }
    else {
        this.sortOrder = 1;
        this.sortField = value;
    }
}



}

