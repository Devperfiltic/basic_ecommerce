import { Component, OnInit } from '@angular/core';
import { Subcategoria } from 'src/model/subcategoria';
import { SubcategoriaService } from '../service/subcategoria.service';
import { SelectItem } from 'primeng/api/selectitem';

@Component({
  selector: 'app-subcategoria',
  templateUrl: './subcategoria.component.html',
  styleUrls: ['./subcategoria.component.css']
})
export class SubcategoriaComponent implements OnInit {

    subcategorias: Subcategoria[]=[]; 
    seleccionado: string;
  
    constructor(private subcategoriaService: SubcategoriaService) { }
  
    ngOnInit() {
      this.subcategoriaService.getAll().subscribe(subcategorias => this.subcategorias = subcategorias);
  
  
    }
  }