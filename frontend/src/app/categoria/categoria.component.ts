import { Component, OnInit } from '@angular/core';
import { Categoria } from 'src/model/categoria';
import { CategoriaService } from '../service/categoria.service';
import { SelectItem } from 'primeng/api/selectitem';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})

export class CategoriaComponent implements OnInit {

  categorias: Categoria[]=[]; 
  seleccionado: string;
  constructor(private categoriaService: CategoriaService) { }

  ngOnInit() {
    this.categoriaService.getAll().subscribe(categorias => this.categorias = categorias);


  }
}


//--------------------------------------------------------------------------------------
/*export class CategoriaComponent implements OnInit {

  categorias: Categoria[]=[]; 

  constructor(private categoriaService: CategoriaService) { }

  ngOnInit() {
    this.categoriaService.getAll().subscribe(categorias => this.categorias = categorias);


  }
}*/


//-----------------------------------------------------------------------------------
 /* getAll(){
    this.categoriaService.getAll().subscribe(
      (result: any) => {

        for (let i = 0; i < result.length; i++) {
          let categoria = result[i] as Categoria;
          this.categorias.push(categoria);          
        }
          },
      error => {
        console.log(error);
      }
      );
  }

  ngOnInit() {
    this.getAll();

  }

}*/

//-------------------------------------------------------------------------------------------------
/*export class CategoriaComponent implements OnInit {

  categorias: Categoria[]; 
  cols: any[];

  constructor(private categoriaService: CategoriaService) { }
  getAll(){
    this.categoriaService.getAll().subscribe(
      (result: any) => {
      let categorias: Categoria[]=[]; 
        for (let i = 0; i < result.length; i++) {
          let categoria = result[i] as Categoria;
          categorias.push(categoria);          
        }
        this.categorias=categorias;
          },
      error => {
        console.log(error);
      }
      );
  }

  ngOnInit() {
    this.getAll();
    this.cols= [
      {field: "idcategoria", header:"ID"},
      {field: "nombrecategoria", header:"Nombre Categoria"},
      {field: "fotocategoria", header:"Foto Categoria"}
    ];

  }

} */

